'use strict';

document.addEventListener('DOMContentLoaded', generateTable);

/**
 * Function that generates the rows and cells to form the table
 */
function generateTable() {
    let rowCountEl = document.getElementById('row-count');
    rowCountEl.addEventListener('click', resetTable);
    
    let rowCount = 2;
    rowCountEl.addEventListener('change', function() {
        rowCount = document.getElementById('row-count').value;
        for (let i = 0; i < rowCount; i++) {
            addHTMLElement('tr',undefined,document.getElementById("table1"));
        }
    });

    let colCountEl = document.getElementById('col-count');
    let colCount = 2;
    let cellNumber = 0;
    colCountEl.addEventListener('change', function() {        
        colCount = document.getElementById('col-count').value;
        for (let i = 0; i < rowCount; i++) {
            for (let j = 0; j < colCount; j++) {
                cellNumber++;
                addHTMLElement('td',cellNumber,document.getElementById("table1").children[i]);
            }
        }
        cellNumber = 0;
    });

    colCountEl.addEventListener('change', generateHTML);
}

/**
 * @description Function to generate the HTML of the dynamically created table
 * All the tags are added into a string which is then put into a new created <p>
 */
function generateHTML() {
    let rowCount = document.getElementById('row-count').value;
    let colCount = document.getElementById('col-count').value;
   
   
    let code = "<table>";
    let cellNumber = 0;
    for (let i = 0; i < rowCount; i++) {
        code = code + "\n \t <tr>";
        for (let j = 0; j < colCount; j++) {
            cellNumber++;
            code = code + "\n \t \t <td>" + cellNumber; + "</td>";
        }
        code = code + "\n \t </tr>";
    }
    cellNumber = 0;
    code = code + "</table>";
    addHTMLElement("p", code, document.getElementById("table-html-space"));
}

/**
 * @description Function to reset the table so that new rows and cells can be added after
 * The rows and column elements are removed using removeChild()
 */
function resetTable() {
    let rowCount = document.getElementById('row-count').value;
    let colCount = document.getElementById('col-count').value;
    if (colCount === "2" && rowCount === "2") {
        document.getElementById("table1").removeChild(document.getElementById("table1").lastElementChild);
    }
    else {
        for (let j = rowCount; j < rowCount; j++) {
            for (let i = colCount-1; i >= 0; i--) {
                document.getElementById("table1").children[j].removeChild(document.querySelectorAll("td")[0]);
            }
        }
        for (let i = rowCount-1; i >= 0; i--) {
            document.getElementById("table1").removeChild(document.querySelectorAll("tr")[i]);
        }
    }
    //document.getElementById("table-html-space").removeChild(document.getElementById("table-html-space").children[0])
};

/**
 * @description A function to add HTML elements using javascript 
 * @param {string} tag the tag name for the element that'll be created
 * @param {string} theText text that might be inside of the element that'll be created
 * @param {HTMLElement} theParent the parent element of the element that'll be created
 */
function addHTMLElement(tag, theText, theParent) {
    let elem = document.createElement(tag);
    if (theText!==undefined) {
        elem.textContent = theText;
    }
    theParent.appendChild(elem);
}

