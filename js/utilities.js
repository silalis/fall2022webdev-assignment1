
'use strict';

document.addEventListener('DOMContentLoaded', styleTable);

/**
 * @description Function which holds the event listeners and handlers to customize the generated table
 */
function styleTable() {

/**
 * Event listener with anonymous function to change the background color of the generated table
 */
let bkColorInput = document.getElementById("background-color");
bkColorInput.addEventListener("change", function() {
    let selector = document.getElementById("table1");
    let backgroundColor = document.getElementById("background-color").value;
    selector.style.background = backgroundColor;
}
);

/**
 * Event listener with anonymous function to change the text color in the generated table
 */
let textColorInput = document.getElementById('text-color');
textColorInput.addEventListener("input", function() {
    let selector = document.getElementById("table1");
    let textColor = document.getElementById('text-color').value;
    selector.style.color = textColor;
});

/**
 * Event listener with anonymous function to change the width of the generated table
 */
let widthInput = document.getElementById('table-width');
widthInput.addEventListener("input", function() {
    let selector = document.getElementById("table1");
    let inputWidth = document.getElementById('table-width').value + '%';
    selector.style.width = inputWidth;
});

/**
 * Event listener with anonymous function to change the border color of the generated table
 */
let borderColorInput = document.getElementById('border-color');
borderColorInput.addEventListener("change", function() {
    let selector = document.getElementById("table1");
    let borderColor = document.getElementById('border-color').value;
    selector.style.borderColor = borderColor;
});

/**
 * Event listener with anonymous function to change the width of the generated table's border
 */
let borderWidthInput = document.getElementById('border-width');
borderWidthInput.addEventListener("input", function() {
    let selector = document.getElementById("table1");
    let borderWidth = document.getElementById('border-width').value;
    selector.style.borderWidth = borderWidth;

});

}
